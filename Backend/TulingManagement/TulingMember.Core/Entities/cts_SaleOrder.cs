﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    [TableAudit]
    public class cts_SaleOrder: DEntityTenant
    {



        public string OrderNo { get; set; }
        /// <summary>
        /// 。
        /// </summary>

        public DateTime? OrderDate { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public long CustomerId { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string CustomerName { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string OrderDesc { get; set; }


        /// <summary>
        /// 件数合计
        /// </summary>
        public int Num { get; set; }

        /// <summary>
        /// 重量合计
        /// </summary>
        public decimal Weight { get; set; }
        
        /// <summary>
        /// 合计。
        /// </summary>

        public decimal Amount { get; set; }
        /// <summary>
        /// 运费
        /// </summary>
        public decimal FreightFee { get; set; }
        /// <summary>
        /// 版费
        /// </summary>
        public decimal EditionFee { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal GiveupFee { get; set; }


        /// <summary>
        /// 最终费用合计
        /// </summary>
        public decimal AllFee { get; set; }

        public decimal PayFee { get; set; }

        /// <summary>
        /// 客户当前欠款
        /// </summary>

        public decimal CustomerBalance { get; set; }
        /// <summary>
        /// 。
        /// </summary>

        public string Remark { get; set; }

        /// <summary>
        /// 状态0.待审核1.已审核
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 打印次数
        /// </summary>
        public int PrintNum { get; set; }

    }
}