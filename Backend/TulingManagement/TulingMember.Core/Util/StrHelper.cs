﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TulingMember.Core
{
    public class StrHelper
    {
        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string Base64Code(byte[] bytes)
        {
            try
            {
                if (bytes==null)
                {
                    return null;
                }
                return Convert.ToBase64String(bytes);
            }
            catch (Exception)
            {

                return "";
            }
            
        }
        /// <summary>
      　/// Base64解密
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static byte[] Base64Decode(string Message)
        {
            try
            {
                if (Message == null)
                {
                    return null;
                }
                byte[] bytes = Convert.FromBase64String(Message);
                return bytes;
            }
            catch (Exception)
            {
                return null;
            }          
           
        } 
    }
}
