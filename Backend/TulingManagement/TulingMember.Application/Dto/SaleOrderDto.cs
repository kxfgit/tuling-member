﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TulingMember.Core;

namespace TulingMember.Application.Dto
{
    public class SaleOrderDto
    {
        public List<cts_PrintTag> printtag { get; set; }
        public string companyname { get; set; }
        public cts_SaleOrder order { get; set; }
        public List<cts_SaleOrderDetail> detail { get; set; }
    }
    public class SaleOrderBackDto
    {
        public List<cts_PrintTag> printtag { get; set; }
        public string companyname { get; set; }
        public cts_SaleOrderBack order { get; set; }
        public List<cts_SaleOrderDetailBack> detail { get; set; }
    }
}
