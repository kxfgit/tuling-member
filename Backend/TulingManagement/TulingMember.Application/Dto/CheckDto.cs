﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Application
{
    public class CheckDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 0.弃审1.审核
        /// </summary>
        public int Status { get; set; }
    }
}
