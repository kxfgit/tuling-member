﻿using EFCore.BulkExtensions;
using Furion;
using Furion.DatabaseAccessor;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Furion.UnifyResult;
using JoyAdmin.Core;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using StackExchange.Profiling.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TulingMember.Application.Dto;
using TulingMember.Core; 
using Yitter.IdGenerator;

namespace TulingMember.Application
{
    [ApiDescriptionSettings("采购单")]
    public class PurchaseOrderService : IDynamicApiController
    {
        private readonly ILogger _logger; 
        private readonly IRepository<cts_Supplier> _supplierRepository;
        private readonly IRepository<cts_SupplierPayLog> _supplierpaylogRepository;
        private readonly IRepository<cts_PurchaseOrder> _purchaseorderRepository;
        private readonly IRepository<cts_PurchaseOrderDetail> _purchaseorderdetailRepository;
        private readonly IRepository<cts_PrintTag> _printtagRepository;
        private readonly IRepository<cts_Product> _productRepository;
        private readonly IRepository<cts_ProductLog> _productlogRepository;
        public PurchaseOrderService(ILogger logger
            , IRepository<cts_Supplier> supplierRepository
            , IRepository<cts_SupplierPayLog> supplierpaylogRepository
            , IRepository<cts_PurchaseOrder> purchaseorderRepository
            , IRepository<cts_PurchaseOrderDetail> purchaseorderdetailRepository
            , IRepository<cts_PrintTag> printtagRepository
            , IRepository<cts_Product> productRepository
            , IRepository<cts_ProductLog> productlogRepository)
        {
            _logger = logger;
            _supplierRepository = supplierRepository;
            _supplierpaylogRepository = supplierpaylogRepository;
            _purchaseorderRepository = purchaseorderRepository;
            _purchaseorderdetailRepository = purchaseorderdetailRepository;
            _printtagRepository = printtagRepository;
            _productRepository = productRepository;
            _productlogRepository = productlogRepository;
        }


        #region 采购单
        /// <summary>
        /// 列表
        /// </summary> 
        public PagedList<cts_PurchaseOrder> SearchPurchaseOrder(BaseInput input)
        {

            var search = _purchaseorderRepository.AsQueryable();
            if (!string.IsNullOrEmpty(input.keyword))
            {
                search = search.Where(m => m.CustomerName.Contains(input.keyword)
                 || m.OrderDesc.Contains(input.keyword));
            }
            if (input.sdate != null)
            {
                search = search.Where(m => m.CreatedTime >= input.sdate);
            }
            if (input.edate != null)
            {
                var edate = input.edate?.AddDays(1).Date;
                search = search.Where(m => m.CreatedTime < edate);
            }
            if (input.supplierid > 0)
            {

                search = search.Where(m => m.CustomerId == input.supplierid);
            }
            var amount = search.GroupBy(m => 1).Select(m => new
            {
                Amount = m.Sum(m => m.Amount),
                FreightFee = m.Sum(m => m.FreightFee),
                EditionFee = m.Sum(m => m.EditionFee),
                GiveupFee = m.Sum(m => m.GiveupFee),
                AllFee = m.Sum(m => m.AllFee),
                PayFee = m.Sum(m => m.PayFee)
            }).FirstOrDefault();
            search = search.OrderByDescending(m => m.OrderDate).OrderByDescending(m => m.OrderNo);
            var data = search.ToPagedList(input.page, input.size);
            UnifyContext.Fill(amount);
            return data;
        }
        /// <summary>
        /// 获取销售单
        /// </summary> 

        public PurchaseOrderDto GetPurchaseOrder(long id)
        {

            var order = _purchaseorderRepository.FindOrDefault(id);
            var detail = _purchaseorderdetailRepository.Where(m => m.OrderId == id).ToList();
            var printtag = _printtagRepository.Where(m => m.IsShow == 1).ToList();
            return new PurchaseOrderDto
            {
                printtag = printtag,
                order = order,
                detail = detail
            };
        }
        /// <summary>
        /// 获取销售单打印
        /// </summary> 

        public PurchaseOrderDto GetPurchaseOrderAndPrint(long id)
        {

            var order = _purchaseorderRepository.FindOrDefault(id);
            var detail = _purchaseorderdetailRepository.Where(m => m.OrderId == id).ToList();
            var printtag = _printtagRepository.Where(m => m.IsShow == 1).ToList();
            //获取系统配置
            var tenantid = App.User.FindFirst(ClaimConst.TENANT_ID).Value;
            cts_SystemConfig systemconfig = new cts_SystemConfig();
            var configKey = AppStr.SystemConfig + "_" + tenantid;
            var systemconfigStr = RedisHelper.GetString(configKey);
            if (!string.IsNullOrEmpty(systemconfigStr))
            {
                systemconfig = systemconfigStr.FromJson<cts_SystemConfig>();
            }
            order.PrintNum += 1;
            return new PurchaseOrderDto
            {
                companyname= systemconfig.configstr1,
                printtag = printtag,
                order = order,
                detail = detail
            };
        }
        /// <summary>
        /// 保存销售单
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [SecurityDefine("purchaseorder")]
        [UnitOfWork]
        public long SavePurchaseOrder(PurchaseOrderDto dto)
        {
            var order = dto.order;
            var detail = dto.detail;
            if (detail.GroupBy(g => g.ProductId).Where(s => s.Count() > 1).Count() > 0)
            {
                throw Oops.Bah("商品重复，请检查").StatusCode(ErrorStatus.ValidationFaild);
            }

            var today = DateTime.Now.ToString("yyyyMMdd");
            var tenantid = App.User.FindFirst(ClaimConst.TENANT_ID).Value;

            //获取系统配置
            cts_SystemConfig systemconfig = new cts_SystemConfig();
            var configKey = AppStr.SystemConfig + "_" + tenantid;
            var systemconfigStr = RedisHelper.GetString(configKey);
            if (!string.IsNullOrEmpty(systemconfigStr))
            {
                systemconfig = systemconfigStr.FromJson<cts_SystemConfig>();
            }

            order.Num = detail.Sum(m => m.Num);
            order.Weight = Math.Round(detail.Sum(m => Math.Round(m.Weight, 2, MidpointRounding.AwayFromZero)), 2, MidpointRounding.AwayFromZero);
            if (systemconfig.config4 == 1)//保留整数
            {
                order.Amount = detail.Sum(m => Math.Floor(m.Price * m.Num));
                order.AllFee = Math.Floor(order.Amount + order.FreightFee + order.EditionFee - order.GiveupFee);

            }
            else
            {
                order.Amount = Math.Round(detail.Sum(m => Math.Round(m.Price * m.Num, 2, MidpointRounding.AwayFromZero)), 2, MidpointRounding.AwayFromZero);
                order.AllFee = Math.Round(order.Amount + order.FreightFee + order.EditionFee - order.GiveupFee, 2, MidpointRounding.AwayFromZero);

            }
            order.OrderDesc = string.Join(';', detail.Select(m => m.ProductName));

            if (order.Id == 0)
            {  
                var redisKey = AppStr.PurchaseOrderNoKey + "_" + tenantid + "_" + today;
                var OrderNoIndex = RedisHelper.StringIncrement(redisKey);
                if (OrderNoIndex == 0)
                {
                    OrderNoIndex = RedisHelper.StringIncrement(redisKey);
                }
                order.Id = YitIdHelper.NextId();
                order.OrderNo = "CG" + today + OrderNoIndex.ToString().PadLeft(3, '0');
                order.Status = 0;
                _purchaseorderRepository.Insert(order);

                foreach (var item in detail)
                {
                    var productName = item.ProductName.Trim(); 
                    item.OrderId = order.Id;
                    item.Id = YitIdHelper.NextId(); 
                    if (systemconfig.config4 == 1)//保留整数
                    {
                        item.Amount = Math.Floor(item.Price * item.Num);
                    }
                    else
                    {
                        item.Amount = Math.Round(item.Price * item.Num, 2, MidpointRounding.AwayFromZero);
                    }
                    _purchaseorderdetailRepository.Insert(item); 


                } 
            }
            else
            {
                         
                var oldOrder = _purchaseorderRepository.DetachedEntities.Where(m => m.Id == order.Id).FirstOrDefault();
                if (oldOrder.Status ==1)
                {
                    throw Oops.Bah("已审核销售单不允许修改").StatusCode(ErrorStatus.ValidationFaild);
                }
                _purchaseorderRepository.UpdateExclude(order,new string[] {nameof(order.PrintNum) });
                _purchaseorderdetailRepository.Where(a => a.OrderId ==order.Id).BatchDelete();
                foreach (var item in detail)
                {
                    var productName = item.ProductName.Trim();
                    item.OrderId = order.Id;
                    item.Id = YitIdHelper.NextId();
                    if (systemconfig.config4 == 1)//保留整数
                    {
                        item.Amount = Math.Floor(item.Price * item.Num);
                    }
                    else
                    {
                        item.Amount = Math.Round(item.Price * item.Num, 2, MidpointRounding.AwayFromZero);
                    }
                    _purchaseorderdetailRepository.Insert(item);
                     
                }
            }
            return order.Id;

        }
        /// <summary>
        /// 删除 
        /// </summary>  
        [SecurityDefine("purchaseorder")]
        public void DeletePurchaseOrder(long id)
        {

            var oldOrder = _purchaseorderRepository.DetachedEntities.Where(m => m.Id == id).FirstOrDefault();
            if (oldOrder.Status == 1)
            {
                throw Oops.Bah("已审核销售单不允许删除").StatusCode(ErrorStatus.ValidationFaild);
            }
            _purchaseorderRepository.FakeDelete(id);
        }
        /// <summary>
        /// 审核、弃审 
        /// </summary>  
        [SecurityDefine("purchaseorder")]
        [UnitOfWork]
        public void CheckPurchaseOrder(CheckDto input)
        {

            var order = _purchaseorderRepository.FindOrDefault(input.Id);
            if (order.Status == input.Status)
            {
                throw Oops.Bah("该单据无需此操作").StatusCode(ErrorStatus.ValidationFaild);
            }
            order.Status=input.Status; 
            var orderDetails=_purchaseorderdetailRepository.Where(m=>m.OrderId==order.Id).ToList();
           

            var changeAmount = Math.Round(order.PayFee - order.AllFee, 2, MidpointRounding.AwayFromZero);
            if (input.Status == 1)//审核
            {
                //1.变更客户金额
                var supplier = _supplierRepository.FindOrDefault(order.CustomerId);   
                _supplierpaylogRepository.Insert(
                    new cts_SupplierPayLog
                    {
                        SupplierId= supplier.Id,
                        SupplierName= supplier.Name,
                        OrderType = OrderType.Out,
                        OrderId=order.Id,
                        OrderNo=order.OrderNo,
                        OldAmount = supplier.Balance,
                        ChangeAmount=changeAmount,
                        NewAmount= supplier.Balance + changeAmount,
                        OrderDate=order.OrderDate, 
                        Remark=$"【采购单】费用：{order.AllFee}元,付款：{order.PayFee}元",
                    });
                supplier.Balance += changeAmount;
                //3.库存数量变更
                foreach (var item in orderDetails)
                {
                    var product = _productRepository.FindOrDefault(item.ProductId);
                   
                    _productlogRepository.Insert(new cts_ProductLog
                    {
                        Id = YitIdHelper.NextId(),
                        ProductId = product.Id,
                        ProductName = product.Name,
                        OldNum = product.Num,
                        ChangeNum = item.Num,
                        NewNum = product.Num + item.Num,
                        TagName = "采购单入库",
                        TagType = ProductChangeTag.In,
                        RelationId =order.Id, 
                    });
                    product.Num += item.Num;
                }

            }
            else
            { //弃审
              //1.变更客户金额
                var supplier = _supplierRepository.FindOrDefault(order.CustomerId); 
                _supplierpaylogRepository.Insert(
                    new cts_SupplierPayLog
                    {
                        SupplierId = supplier.Id,
                        SupplierName = supplier.Name,
                        OrderType = OrderType.Out,
                        OrderId = order.Id,
                        OrderNo = order.OrderNo,
                        OldAmount = supplier.Balance,
                        ChangeAmount = -changeAmount,
                        NewAmount = supplier.Balance - changeAmount,
                        OrderDate = order.OrderDate,
                        Remark = "采购单弃审",
                    });
                supplier.Balance -= changeAmount;
                //3.库存数量变更
                foreach (var item in orderDetails)
                {
                    var product = _productRepository.FindOrDefault(item.ProductId);

                    _productlogRepository.Insert(new cts_ProductLog
                    {
                        Id = YitIdHelper.NextId(),
                        ProductId = product.Id,
                        ProductName = product.Name,
                        OldNum = product.Num,
                        ChangeNum = -item.Num,
                        NewNum = product.Num - item.Num,
                        TagName = "采购单入库弃审",
                        TagType = ProductChangeTag.Out,
                        RelationId = order.Id,
                    });
                    product.Num -= item.Num;
                }
            } 
        }
        #endregion



    }
}
